
// Update Cart Total
function updateCartTotal() {
    let cartTotal = 0;

    $('.cart-subtotal').each(function() {
        cartTotal += parseInt($(this).text());
    });

    cartTotal = cartTotal.toFixed(2);

    $('.cart-total').text(`Total $${cartTotal}`);

}

// Validate Email
function validateEmail(email) {

    var isValidEmail;
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    isValidEmail = (re.test(String(email).toLowerCase()) && email.split('@')[0].length <= 64);

    if(!isValidEmail) {
        return 'Invalid Email Address!';
    }

    return '';

}

