
$(document).ready(function() {

    updateCartTotal();

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    // Add Products To Cart
    $(document).on('click', '.product-item .add-to-cart', function() {

        let self = $(this);
        let data = {
            product_id: self.closest('.product-item').data('id'),
            quantity: 1
        };

        $.ajax({
            url: "/cart",
            method: "POST",
            data: data,
            dataType: "json",
            success: function(data) {
                self.html('Remove From Cart');
                self.toggleClass('add-to-cart remove-from-cart btn-success btn-danger');
            },
            error: function() {
                console.log('error');
            },
            done: function() {
                console.log('done');
            }
        });

    });

    // Add Products To Cart From Single Product Page
    $(document).on('click', '.single-product .add-to-cart', function() {

        let self = $(this);
        let data = {
            product_id: self.closest('.single-product').data('id'),
            quantity: $('#product-quantity').val()
        };

        $.ajax({
            url: "/cart",
            method: "POST",
            data: data,
            dataType: "json",
            success: function(data) {
                $('#product-quantity').val(0);
                console.log('success');
            },
            error: function() {
                console.log('error');
            },
            done: function() {
                console.log('done');
            }
        });

    });

    // Remove Products From Cart
    $(document).on('click', '.remove-from-cart', function() {

        let self = $(this);
        let data = {
            product_id: self.closest('.product-item').data('id')
        };

        let deleteType = $(this).data('type');

        $.ajax({
            url: "/cart",
            method: "DELETE",
            data: data,
            dataType: "json",
            success: function(data) {

                if(deleteType === 'cart') {
                    self.closest('.cart-item').remove();

                    updateCartTotal();
                } else {
                    self.html('Add To Cart');
                    self.toggleClass('add-to-cart remove-from-cart btn-success btn-danger');
                }

            },
            error: function() {
                console.log('error');
            },
            done: function() {
                console.log('done');
            }
        });

    });

    $(document).on('change', '.cart-quantity', function() {

        let thisSubtotal = $(this).closest('.cart-item').find('.cart-subtotal');
        let thisPrice = $(this).closest('.cart-item').find('.cart-price').text();
        let thisQuantity = $(this).val();
        let productId = $(this).closest('.cart-item').data('id');
        if(!thisQuantity || thisQuantity < 0) {
            $(this).val(0);
            thisQuantity = 0;
        }
        thisSubtotal.text((thisQuantity * thisPrice).toFixed(2));

        let data = {
            quantity: thisQuantity,
            product_id: productId
        };

        $.ajax({
            url: "/cart",
            method: "PUT",
            data: data,
            dataType: "json",
            success: function(data) {

            },
            error: function() {
                // console.log('error');
            },
            done: function() {
                // console.log('done');
            }
        });

        updateCartTotal();

    });

    // Single Product Quantity Form
    $(".single-product .btn-minus").on("click",function(){
        let now = $("#product-quantity").val();
        if ($.isNumeric(now)){
            if (parseInt(now) -1 > 0){ now--;}
            $("#product-quantity").val(now);
        } else{
            $("#product-quantity").val("1");
        }
    });

    $(".single-product .btn-plus").on("click",function(){
        let now = $("#product-quantity").val();
        let inStock = $('#in-stock').val();

        if ($.isNumeric(now) && parseInt(now) < inStock) {
            $("#product-quantity").val(parseInt(now)+1);
        } else if(parseInt(now) >= inStock) {
            $("#product-quantity").val(inStock);
        } else{
            $("#product-quantity").val("1");
        }
    });

    $('#product-quantity').change(function() {
        let inStock = $('#in-stock').val();
        let thisVal = $(this).val();

        if(thisVal < 1 || !thisVal) {
            $(this).val(1);
        } else if(parseInt(thisVal) > inStock) {
            $("#product-quantity").val(inStock);
        }
    });

    // User Create/Update Validation With Ajax
    $(document).on('change blur', '.user-inputs', function() {

        var type = $(this).closest('form').data('type');
        var userId = $('#hidden').val();

        var name = $('#name').val();
        var email = $('#email').val();
        var password = $('#password').val();

        var data = {
            name: name,
            email: email,
            password: password,
            type: type,
            user_id: userId ? userId : 0
        };

        for (var key in data) {
            if(!data[key] && !$('#' + key).data('not-required')) {
                $('#error-' + key).text('The ' + key + ' field is required.');
                $('#submit').attr('disabled', 'disabled');
            }
        }

        $('#errors-email').text(validateEmail(email));

        // Validate All Inputs
        $.ajax({
            url: "/email/validate",
            method: "POST",
            data: data,
            dataType: "json",
            success: function(result) {
                var isDisabled = false;
                $.each(data, function(key, value){
                    var message = '';
                    if (result.errors && key in result.errors) {
                        message = result.errors[key];
                        isDisabled = true;
                    }
                    $('#error-' + key).text(message);
                });
                if(!isDisabled) {
                    $('#submit').removeAttr('disabled');
                } else {
                    $('#submit').attr('disabled', 'disabled');
                }
            },
            error: function() {
                console.log('error');
            },
            done: function() {
                console.log('done');
            }
        });

    });

    $(document).on('change blur', '.number-input', function() {

        let thisVal = $(this).val();

        if(!thisVal || thisVal < 0) {
            $(this).val(0);
        }

    });

});