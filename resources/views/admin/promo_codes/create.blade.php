@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.promo-codes.title')</h3>
    {!! Form::open(['method' => 'POST', 'route' => ['admin.promo_codes.store'], 'id' => 'create-promo-codes-form']) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_create')
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('name', trans('quickadmin.promo-codes.fields.name').'*', ['class' => 'control-label']) !!}
                    {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => 'Promo code name', 'required' => 'required']) !!}
                    {{--<p class="help-block"></p>--}}
                    {{--@if($errors->has('name'))--}}
                        {{--<p class="help-block">--}}
                            {{--{{ $errors->first('name') }}--}}
                        {{--</p>--}}
                    {{--@endif--}}
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('description', trans('quickadmin.promo-codes.fields.description'), ['class' => 'control-label']) !!}
                    {!! Form::text('description', old('description'), ['class' => 'form-control', 'placeholder' => 'Description', 'required' => 'required']) !!}
                    {{--<p class="help-block"></p>--}}
                    {{--@if($errors->has('description'))--}}
                        {{--<p class="help-block">--}}
                            {{--{{ $errors->first('description') }}--}}
                        {{--</p>--}}
                    {{--@endif--}}
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::radio('is_fixed', 1, true, ['id' => 'type-1']) !!}
                    {!! Form::label('type-1', trans('quickadmin.promo-codes.fields.fixed'), ['class' => 'control-label radio-margin']) !!}
                    {!! Form::radio('is_fixed', 0, false, ['id' => 'type-0']) !!}
                    {!! Form::label('type-0', trans('quickadmin.promo-codes.fields.percent'), ['class' => 'control-label radio-margin']) !!}

                    {{--<p class="help-block"></p>--}}
                    {{--@if($errors->has('is_fixed'))--}}
                        {{--<p class="help-block">--}}
                            {{--{{ $errors->first('is_fixed') }}--}}
                        {{--</p>--}}
                    {{--@endif--}}
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('amount', trans('quickadmin.promo-codes.fields.amount').'*', ['class' => 'control-label']) !!}
                    {!! Form::number('discount_amount', old('amount'), ['class' => 'form-control', 'placeholder' => 'Discount amount', 'required' => 'required']) !!}
                    {{--<p class="help-block"></p>--}}
                    {{--@if($errors->has('discount_amount'))--}}
                        {{--<p class="help-block">--}}
                            {{--{{ $errors->first('discount_amount') }}--}}
                        {{--</p>--}}
                    {{--@endif--}}
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('max_uses', trans('quickadmin.promo-codes.fields.max_uses'), ['class' => 'control-label']) !!}
                    {!! Form::number('max_uses', old('max_uses'), ['class' => 'form-control', 'placeholder' => 'Max uses', 'required' => '']) !!}
                    {{--<p class="help-block"></p>--}}
                    {{--@if($errors->has('max_uses'))--}}
                        {{--<p class="help-block">--}}
                            {{--{{ $errors->first('max_uses') }}--}}
                        {{--</p>--}}
                    {{--@endif--}}
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('max_uses_user', trans('quickadmin.promo-codes.fields.max_uses_user'), ['class' => 'control-label']) !!}
                    {!! Form::number('max_uses_user', old('max_uses_user'), ['class' => 'form-control', 'placeholder' => 'Max user uses', 'required' => '']) !!}
                    {{--<p class="help-block"></p>--}}
                    {{--@if($errors->has('max_uses_user'))--}}
                        {{--<p class="help-block">--}}
                            {{--{{ $errors->first('max_uses_user') }}--}}
                        {{--</p>--}}
                    {{--@endif--}}
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('start', trans('quickadmin.promo-codes.fields.start'), ['class' => 'control-label']) !!}
                    {!! Form::date('starts_at', old('start'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    {{--<p class="help-block"></p>--}}
                    {{--@if($errors->has('starts_at'))--}}
                        {{--<p class="help-block">--}}
                            {{--{{ $errors->first('starts_at') }}--}}
                        {{--</p>--}}
                    {{--@endif--}}
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('end', trans('quickadmin.promo-codes.fields.end'), ['class' => 'control-label']) !!}
                    {!! Form::date('expires_at', old('end'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    {{--<p class="help-block"></p>--}}
                    {{--@if($errors->has('expires_at'))--}}
                        {{--<p class="help-block">--}}
                            {{--{{ $errors->first('expires_at') }}--}}
                        {{--</p>--}}
                    {{--@endif--}}
                </div>
            </div>

        </div>
    </div>

    {!! Form::submit(trans('quickadmin.qa_save'), ['class' => 'btn btn-success']) !!}
    {!! Form::close() !!}
@stop