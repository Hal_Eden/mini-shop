@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.users.title')</h3>
    
    {!! Form::model($user, ['method' => 'PUT', 'route' => ['admin.users.update', $user->id], 'data-type' => 'update'] ) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_edit')
        </div>

        <div class="panel-body">
            <div class="row">
                {!! Form::hidden('hidden', $user->id, ['id' => 'hidden']) !!}
                <div class="col-xs-12 form-group">
                    {!! Form::label('name', trans('quickadmin.users.fields.name').'*', ['class' => 'control-label']) !!}
                    {!! Form::text('name', old('name'), ['class' => 'form-control user-inputs', 'id' => 'name', 'placeholder' => '', 'autocomplete' => 'new-name', 'required' => '']) !!}
                    <p class="help-block" id="error-name">
                        @if($errors->has('name'))
                            {{ $errors->first('name') }}
                        @endif
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('email', trans('quickadmin.users.fields.email').'*', ['class' => 'control-label']) !!}
                    {!! Form::email('email', old('email'), ['class' => 'form-control user-inputs', 'id' => 'email', 'placeholder' => '', 'autocomplete' => 'new-email', 'required' => '']) !!}
                    <p class="help-block" id="error-email">
                        @if($errors->has('email'))
                            {{ $errors->first('email') }}
                        @endif
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('password', trans('quickadmin.users.fields.password').'*', ['class' => 'control-label']) !!}
                    {!! Form::password('password', ['class' => 'form-control user-inputs', 'id' => 'password', 'placeholder' => '', 'autocomplete' => 'new-password', 'data-not-required' => 'true']) !!}
                    <p class="help-block" id="error-password">
                        @if($errors->has('password'))
                            {{ $errors->first('password') }}
                        @endif
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('role_id', trans('quickadmin.users.fields.role').'*', ['class' => 'control-label']) !!}
                    {!! Form::select('role_id', $roles, old('role_id'), ['class' => 'form-control select2 user-inputs', 'id' => 'role', 'autocomplete' => 'new-role', 'required' => '']) !!}
                    <p class="help-block" id="error-role">
                        @if($errors->has('role_id'))
                            {{ $errors->first('role_id') }}
                        @endif
                    </p>
                </div>
            </div>
            
        </div>
    </div>

    {!! Form::submit(trans('quickadmin.qa_update'), ['class' => 'btn btn-success', 'id' => 'submit']) !!}
    {!! Form::close() !!}
@stop

