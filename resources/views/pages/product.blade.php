@extends('layouts.master')

@section('content')
    <h3>Single Product</h3>
    <div class="container">
        <div>
            <div class="row single-product" data-id="{{ $product->id }}">
                <div class="col-xs-4 item-photo">
                    <img style="max-width:100%;" src="{{ asset($product->photo1) }}" />
                </div>
                <div class="col-xs-5" style="border:0px solid gray">
                    <h3>{{ $product->name }}</h3>
                    <h5 style="color:#337ab7">{{ $product->description }}</h5>

                    <input type="hidden" id="in-stock" value="{{ $product->stock }}">

                    <div class="row">
                        <div class="col-xs-6">
                            <h6 class="title-price title-attr">Price</h6>
                            <h3 style="margin-top:0px;">$<span class="product-price">{{ $product->price }}</span></h3>
                        </div>
                        <div class="col-xs-6">
                            <h6 class="title-price title-attr">Availability</h6>
                            <h3 style="margin-top:0px;"><span class="product-price {{ $product->stock ? '' : 'text-danger' }}">{{ $product->stock ? $product->stock . ' left' : 'Out Of Stock' }}</span></h3>
                        </div>
                    </div>

                    <div class="section" style="padding-bottom:20px;">
                        <h6 class="title-attr">Quantity</h6>
                        <div>
                            <div class="btn-minus"><span class="glyphicon glyphicon-minus"></span></div>
                            <input id="product-quantity" value="1" />
                            <div class="btn-plus"><span class="glyphicon glyphicon-plus"></span></div>
                        </div>
                    </div>
                    <div class="section" style="padding-bottom:20px;">
                        <button class="btn btn-success add-to-cart"><span style="margin-right:20px" class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span>Add To Cart</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
