@extends('layouts.master')

@section('content')
    <div class="container">
        <table id="cart" class="table table-hover table-condensed">
            <thead>
            <tr>
                <th style="width:50%">Product</th>
                <th style="width:10%">Price</th>
                <th style="width:8%">Quantity</th>
                <th style="width:22%" class="text-center">Subtotal</th>
                <th style="width:10%"></th>
            </tr>
            </thead>
            <tbody>
            @foreach($products as $product)
                <tr class="cart-item product-item" data-id="{{ $product->id }}">
                    <td data-th="Product">
                        <div class="row">
                            <div class="col-sm-2 hidden-xs">
                                <img src="{{ asset($product->photo1) }}" alt="..." class="img-responsive"/>
                            </div>
                            <div class="col-sm-10">
                                <h4 class="nomargin"><a href="{{ route('products.show', $product->id) }}">{{ $product->name }}</a></h4>
                                <p>{{ $product->description }}</p>
                            </div>
                        </div>
                    </td>
                    <td data-th="Price">$<span class="cart-price">{{ $product->price }}</span></td>
                    <td data-th="Quantity">
                        @php
                            $quantity = $cart->where('product_id', $product->id)->first()->quantity;
                        @endphp
                        <input type="number" min="0" class="form-control text-center cart-quantity" value="{{ $quantity }}">
                    </td>
                    <td data-th="Subtotal" class="text-center">$<span class="cart-subtotal">{{ number_format($quantity * $product->price, 2) }}</span></td>
                    <td class="actions" data-th="">
                        <button class="btn btn-danger btn-sm remove-from-cart" data-type="cart"><i class="fa fa-trash-o"></i></button>
                    </td>
                </tr>
            @endforeach
            </tbody>
            <tfoot>
            <tr class="visible-xs">
                <td class="text-center"><strong class="cart-total"></strong></td>
            </tr>
            <tr>
                <td><a href="{{ route('products.index') }}" class="btn btn-warning"><i class="fa fa-angle-left"></i> Continue Shopping</a></td>
                <td colspan="2" class="hidden-xs"></td>
                <td class="hidden-xs text-center"><strong class="cart-total"></strong></td>
                <td><a href="#" class="btn btn-success btn-block">Checkout <i class="fa fa-angle-right"></i></a></td>
            </tr>
            </tfoot>
        </table>
    </div>
@endsection
