@extends('layouts.master')

@section('content')
    <h3>Products List</h3>
    <div class="container">
        <div class="row">
            <div class="col-10 offset-1">
                <div class="row">
                    @foreach($products as $product)
                        <div class="col-lg-3 col-md-4 product-item" data-id="{{ $product->id }}">
                            <div class="card h-100">
                                <a href="{{ route('products.show', $product->id) }}">
                                    <img class="card-img-top" src="{{ asset($product->photo1) }}" alt="">
                                </a>
                                <div class="card-body">
                                    <h4 class="card-title">
                                        <a href="{{ route('products.show', $product->id) }}">{{ $product->name }}</a>
                                    </h4>
                                    <h5>${{ $product->price }}</h5>
                                    <p class="card-text">{{ $product->description }}</p>
                                </div>
                                <div class="card-footer">
                                    <button data-type="home" class="btn btn-sm {{ $user->hasInCart($product->id) ? 'btn-danger remove-from-cart' : 'btn-success add-to-cart' }}">{{ $user->hasInCart($product->id) ? 'Remove From Cart' : 'Add To Cart' }}</button>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
