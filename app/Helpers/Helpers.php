<?php

//Generate Random Promo Code
if (!function_exists('generateCouponCode')) {
    function generateCouponCode() {
        $length = 30;
        $chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $ret = '';
        for($i = 0; $i < $length; ++$i) {
            $random = str_shuffle($chars);
            $ret .= $random[0];
        }
        return $ret;
    }
}