<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Role;

class MiscController extends Controller
{
    public function validateEmail(Request $request) {

        $type = $request->type;
        $user_id = $request->user_id;

        switch($type) {
            case 'create':
                $validator = Validator::make($request->all(), [
                    'name' => 'required|string|max:255',
                    'email' => 'required|string|email|max:255|unique:users',
                    'password' => 'required|string|min:6',
                ]);
                break;
            case 'update':
                $validator = Validator::make($request->all(), [
                    'name' => 'required|string|max:255',
                    'email' => 'required|string|email|max:255|unique:users,email,' . $user_id . ',id',
                    'password' => 'nullable|string|min:6',
                ]);
                break;
        }

        if ($validator->fails())
        {
            return response()->json(['isValid' => false, 'errors' => $validator->errors()]);
        }

        return response()->json(['isValid' => true]);

    }
}
