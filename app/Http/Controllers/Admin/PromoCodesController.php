<?php

namespace App\Http\Controllers\Admin;

use App\PromoCode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StorePromoCodesRequest;
use App\Http\Requests\Admin\UpdatePromoCodesRequest;

class PromoCodesController extends Controller
{
    /**
     * Display a listing of PromoCode.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request('show_deleted') == 1) {
            if (! Gate::allows('promo_code_delete')) {
                return abort(401);
            }
            $promo_codes = PromoCode::onlyTrashed()->get();
        } else {
            $promo_codes = PromoCode::all();
        }

        return view('admin.promo_codes.index', compact('promo_codes'));
    }

    /**
     * Show the form for creating new PromoCode.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.promo_codes.create');
    }

    /**
     * Store a newly created PromoCode in storage.
     *
     * @param  \App\Http\Requests\StorePromoCodesRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePromoCodesRequest $request)
    {
        $code = generateCouponCode();
        $promo_code = PromoCode::create(array_merge($request->all(), ['index' => 'value'], ['code' => $code]));
//        dd($promo_code);

        return redirect()->route('admin.promo_codes.index');
    }


    /**
     * Display PromoCode.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $promo_code = PromoCode::findOrFail($id);

        return view('admin.promo_codes.show', compact('promo_code'));
    }


    /**
     * Remove PromoCode from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $promo_code = PromoCode::findOrFail($id);
        $promo_code->delete();

        return redirect()->route('admin.promo_codes.index');
    }

    /**
     * Delete all selected PromoCode at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if ($request->input('ids')) {
            $entries = PromoCode::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }


    /**
     * Restore PromoCode from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $promo_code = PromoCode::onlyTrashed()->findOrFail($id);
        $promo_code->restore();

        return redirect()->route('admin.promo_codes.index');
    }

    /**
     * Permanently delete PromoCode from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function perma_del($id)
    {
        if (! Gate::allows('promo_code_delete')) {
            return abort(401);
        }
        $promo_code = PromoCode::onlyTrashed()->findOrFail($id);
        $promo_code->forceDelete();

        return redirect()->route('admin.promo_codes.index');
    }
}