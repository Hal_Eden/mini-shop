<?php

namespace App\Http\Controllers\Api\V1;

use App\PromoCode;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StorePromoCodesRequest;
use App\Http\Requests\Admin\UpdatePromoCodesRequest;

class PromoCodesController extends Controller
{
    public function index()
    {
        return PromoCode::all();
    }

    public function show($id)
    {
        return PromoCode::findOrFail($id);
    }

    public function update(UpdatePromoCodesRequest $request, $id)
    {
        $promo_code = PromoCode::findOrFail($id);
        $promo_code->update($request->all());


        return $promo_code;
    }

    public function store(StorePromoCodesRequest $request)
    {
        $promo_code = PromoCode::create($request->all());


        return $promo_code;
    }

    public function destroy($id)
    {
        $promo_code = PromoCode::findOrFail($id);
        $promo_code->delete();
        return '';
    }
}