<?php

namespace App\Http\Controllers;

use App\CartItem;
use Illuminate\Http\Request;

class CartController extends Controller
{

//    Show Cart Form
    public function index() {

        $user = auth()->user();
        $cart = $user->cartItems;
        $cart_product_ids = $cart->pluck('product_id')->toArray();
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', 'http://mini-shop.ec/api/cart', [
            'query' => $cart_product_ids
        ]);

        $products = json_decode($response->getBody())->data;

        return view('pages.cart', compact('products', 'user', 'cart'));

    }

//    Add/Update Cart Products
    public function addToCart(Request $request) {

        CartItem::updateOrInsert(
            [
                'user_id' => auth()->user()->id,
                'product_id' => $request->product_id
            ],
            [
                'quantity' => $request->quantity
            ]
        );

        return response()->json(['success' => true]);

    }

//    Remove A Product From Cart
    public function removeFromCart(Request $request) {

        CartItem::where('user_id', auth()->user()->id)
            ->where('product_id', $request->product_id)
            ->delete();

        return response()->json(['success' => true]);

    }

//    Update Cart Item
    public function updateCart(Request $request) {

        CartItem::where('user_id', auth()->user()->id)
            ->where('product_id', $request->product_id)
            ->update(['quantity' => $request->quantity]);

        return response()->json(['success' => true]);

    }

}
