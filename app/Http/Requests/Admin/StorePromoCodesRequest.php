<?php
namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class StorePromoCodesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'expires_at.after' => 'Expires at date must be greater than starts at.',
            'max_uses_user.min'  => 'The max user uses may not be greater than max uses.',
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'description' => 'string|max:255',
            'is_fixed' => 'required',
            'discount_amount' => 'required|integer|min:1|max:100',
            'max_uses' => 'required|integer|min:1',
            'max_uses_user' => 'required|integer|max:' . ($this->max_uses ?: '1'),
            'starts_at' => 'required|date',
            'expires_at' => 'required|date|after:starts_at'
        ];
    }
}