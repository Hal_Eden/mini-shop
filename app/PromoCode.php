<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class PromoCode
 *
 * @package App
 * @property string $promo
 */
class PromoCode extends Model
{
    use SoftDeletes;

    protected $fillable = ['code', 'name', 'description', 'type', 'uses', 'max_uses', 'max_uses_user', 'discount_amount', 'is_fixed', 'starts_at', 'expires_at'];
    protected $hidden = [];

}