<?php

Route::group(['prefix' => '/v1', 'namespace' => 'Api\V1', 'as' => 'api.'], function () {
    Route::resource('promo_codes', 'PromoCodesController', ['except' => ['create', 'edit']]);
});

//Products CRUD Routes
Route::resource('products', 'Api\ProductsController');

Route::get('cart', 'Api\ProductsController@cart');