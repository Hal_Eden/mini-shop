<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    if(auth()->user()->hasRole('common')) {
        return redirect('/home');
    }

    return redirect('/admin/home');

})->middleware('auth');

// Authentication Routes...
$this->get('login', 'Auth\LoginController@showLoginForm')->name('login');
$this->post('login', 'Auth\LoginController@login')->name('auth.login');
$this->post('logout', 'Auth\LoginController@logout')->name('auth.logout');

// Change Password Routes...
$this->get('change_password', 'Auth\ChangePasswordController@showChangePasswordForm')->name('auth.change_password');
$this->patch('change_password', 'Auth\ChangePasswordController@changePassword')->name('auth.change_password');

// Password Reset Routes...
$this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('auth.password.reset');
$this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('auth.password.reset');
$this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
$this->post('password/reset', 'Auth\ResetPasswordController@reset')->name('auth.password.reset');

Route::group(['middleware' => ['auth', 'role:admin'], 'prefix' => 'admin', 'as' => 'admin.'], function () {
    Route::get('/home', 'HomeController@index');
    
//    Route::resource('roles', 'Admin\RolesController');
//    Route::post('roles_mass_destroy', ['uses' => 'Admin\RolesController@massDestroy', 'as' => 'roles.mass_destroy']);
    Route::resource('users', 'Admin\UsersController');
    Route::post('users_mass_destroy', ['uses' => 'Admin\UsersController@massDestroy', 'as' => 'users.mass_destroy']);
    Route::resource('product_categories', 'Admin\ProductCategoriesController');
    Route::post('product_categories_mass_destroy', ['uses' => 'Admin\ProductCategoriesController@massDestroy', 'as' => 'product_categories.mass_destroy']);
    Route::resource('product_tags', 'Admin\ProductTagsController');
    Route::post('product_tags_mass_destroy', ['uses' => 'Admin\ProductTagsController@massDestroy', 'as' => 'product_tags.mass_destroy']);
    Route::resource('products', 'Admin\ProductsController');
    Route::post('products_mass_destroy', ['uses' => 'Admin\ProductsController@massDestroy', 'as' => 'products.mass_destroy']);

    Route::resource('promo_codes', 'Admin\PromoCodesController');
    Route::post('promo_codes_mass_destroy', ['uses' => 'Admin\PromoCodesController@massDestroy', 'as' => 'promo_codes.mass_destroy']);
    Route::post('promo_codes_restore/{id}', ['uses' => 'Admin\PromoCodesController@restore', 'as' => 'promo_codes.restore']);
    Route::delete('promo_codes_perma_del/{id}', ['uses' => 'Admin\PromoCodesController@perma_del', 'as' => 'promo_codes.perma_del']);

});

Route::group(['middleware' => ['auth', 'role:common']], function () {
//    Product Routes
    Route::get('/home', 'MainController@index');
    Route::get('/products', 'MainController@index')->name("products.index");
    Route::get('/products/{id}', 'MainController@show')->name("products.show");

//    Cart Routes
    Route::get('/cart', 'CartController@index')->name("cart.show");
    Route::post('/cart', 'CartController@addToCart')->name("cart.add");
    Route::delete('/cart', 'CartController@removeFromCart')->name("cart.remove");
    Route::put('/cart', 'CartController@updateCart')->name("cart.update");
});

//Validation Routes
Route::post('/email/validate', 'MiscController@validateEmail');
