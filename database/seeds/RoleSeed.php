<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RoleSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $role_admin = Role::create(['name' => 'admin']);
        $role_common = Role::create(['name' => 'common']);
        $manage_products = Permission::create(['name' => 'manage_products']);
        $manage_users = Permission::create(['name' => 'manage_users']);
        $role_admin->givePermissionTo($manage_products);
        $role_admin->givePermissionTo($manage_users);

    }
}
