<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\User;

class UserSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = ['id' => 1, 'name' => 'Admin', 'email' => 'admin@admin.com', 'password' => Hash::make('admin'), 'remember_token' => ''];

        $admin = User::create($admin);
        $admin->assignRole('admin');
    }
}
