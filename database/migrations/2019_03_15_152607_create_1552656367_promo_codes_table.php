<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1552656367PromoCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('promo_codes', function (Blueprint $table) {
            $table->bigIncrements('id');

            // The voucher code
            $table->string('code');

            // The human readable promo code name
            $table->string('name');

            // The description of the promo code - Not necessary
            $table->text('description')->nullable();

            // The number of uses currently
            $table->integer('uses')->default('0');

            // The max uses this promo code has
            $table->integer('max_uses')->default('1');

            // How many times a user can use this promo code.
            $table->integer('max_uses_user')->default('1');

            // The amount to discount.
            $table->integer('discount_amount');

            // Whether or not the promo code is a percentage or a fixed price.
            $table->boolean('is_fixed')->default(true);

            // When the promo code begins
            $table->timestamp('starts_at');

            // When the promo code ends
            $table->timestamp('expires_at');

            $table->timestamps();

            $table->softDeletes();
        });

        Schema::create('user_promo_codes', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->bigInteger('promo_codes_id')->unsigned();

            $table->unique(['user_id', 'promo_codes_id']);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promo_codes');
    }
}